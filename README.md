# README #

This repository shows users how to use Mock-Server and Gatling together to run a sample performance test and retrieve results from the tests.

### What is this repository for? ###

* Performance testing an HTTP endpoint
* Version 1.0.0

### How do I get set up? ###

* Import the project into eclipse
* Make sure that eclipse knows how to [read scala files](https://stackoverflow.com/questions/8522149/eclipse-not-recognizing-scala-code)
* Within the launchers folder users can run the `clean install -Pperformance`
* **Note:** The `singleThreadedLaunchExample.launch` will not work since the server is not active but it is there to show users how to invoke a standalone Gatling test in Eclipse.
* **Note:** The multi threaded Gatling test (Test1) will fail scenario #2 this is just so that users can see what a failed test graph looks like

### Project Setup ####

* The simulations are stored in `src/test/scala` folder of the application
* Once the simulation is run the graphs are stored in `target/gatling/results/xxxx/index.html`
* Any browser can view this graph and demonstrate what the test results where

### What is this accomplishing ###

This project is to show how easy it is to run a standalone netty server that exposes a URL and sends an HTTP 200 response code. Combining this with the strength of Gatling allows users to see how speed that a server can respond to invocations over a period of time. Not only can it monitory speed but it can also monitor successful invocations, anything other then a HTTP 200 or HTTP 302 can be considered a failed invocation. Once the test completes the [Gatling](https://gatling.io/docs/2.3/extensions/maven_plugin/) produces easy to read grapghs describing the results of every invocation. [Mock Server](http://www.mock-server.com/mock_server/creating_expectations.html) allows users to write Java code with validation to check weither or not to respond to an invocation or to ignore it. Since the code is written a Java Test class any of the Apache technology can be used in the class to generate random responses.

With this users can take a Gatling scala class and point it to their application endpoints and perform a load test and view the results of the test. This alleviates the problem of knowing your application works with manual. Using a few requests here and there to run a test. Now Gatling allows developers to see what the application can do prior to being exposed to the public.

### Who do I talk to? ###

* [Patrique](mailto:patrique.legault@gmail.com)