package com.performance.testing

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import io.gatling.http.config.HttpProtocolBuilder.toHttpProtocol
import io.gatling.http.request.builder.AbstractHttpRequestBuilder.toActionBuilder

class multiThreadedExample extends Simulation {

	val httpProtocol = http
			.baseURL("http://localhost:8080")
			.authorizationHeader("Basic YWRtaW5pc3RyYXRvcjpwYXNzd29yZA==")
			.inferHtmlResources()
			.check(status.is(200))
			

			val scenario1 = scenario("RecordedSimulation1")
			.exec(http("request_0")
					.get("/gatling")
					.queryParam("inString","Patrique"))
					
			val scenario2 = scenario("RecordedSimulation2")
			.exec(http("request_1")
					.get("/gatling_not_real_url")
					.queryParam("inString","DanB"))
					
			//Within this setUp you can add as many scenario as possible they will be run simultaniously 
			setUp(
			    scenario1.inject(
			        nothingFor(4 seconds),
			        splitUsers(100) into atOnceUsers(10) separatedBy(10 seconds)
			        ).protocols(httpProtocol),
			     scenario2.inject(
			        nothingFor(4 seconds),
			        splitUsers(100) into atOnceUsers(10) separatedBy(10 seconds)
			        ).protocols(httpProtocol)
			)
			
}