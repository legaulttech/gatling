package com.performance.testing

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import io.gatling.http.check.HttpCheck
import scala.util.Random

class singleThreadedExample extends Simulation {
  
  //Create an http object which will be used for the simulation
  //this http object also has logic attached to it used to validate
  //if the response made from the request is valid
  //The body response must be less than 20 or else it is considered a fail
	val httpProtocol = http
			.baseURL("http://localhost:8080")
			.authorizationHeader("Basic YWRtaW5pc3RyYXRvcjpwYXNzd29yZA==")
			.inferHtmlResources()
			.check(status.is(200))
	    .check(bodyString.transform(string => string.length).lessThan(70))
			
	    //In this scenario we are going to perform an 
	    //http get with the single parameter called inString to 
	    //a AEM Workflow, we are passing a random string into the
	    //URL, the length is not dynamic simply the content of the 
	    //20 string character
			val scenario1 = scenario("RecordedSimulation1")
			.exec(http("request_0")
					.get("/gatling")
					.queryParam("inString",Random.nextString(Random.nextInt(15))))

			//More on setUp can be found here https://gatling.io/docs/2.3/general/simulation_setup/ 
			setUp(
			    scenario1.inject(
			        nothingFor(4 seconds),
			        splitUsers(100) into atOnceUsers(10) separatedBy(10 seconds)
			        )
			).protocols(httpProtocol)
}