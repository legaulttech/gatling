package com.legaulttech.mockserver;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.Not.not;
import junit.framework.TestCase;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.model.NottableString;

/*
 * A class used to spawn a Mock-Server instance on port 8080 and listen to a context path
 * with a specific query string parameter.
 * 
 * It then returns a 200 message with the message saying "Invoked Mock-Server"
 * 
 * @author Legault.Tech
 */
public class MockServerTest extends TestCase
{
	@Test
	public void testMockServer()
	{
		new MockServerClient("127.0.0.1", 8080)
			.when(
				request()
					.withMethod("GET")
					.withPath("/gatling")
					.withQueryStringParameter(NottableString.string("inString"), NottableString.not(RandomStringUtils.random((int)(50 * Math.random()), "qazwsxedcrfvtgbyhnujmikolp")))
				)
				.respond(
					response()
						.withStatusCode(200)
						.withBody("Invoked the server - thank you: " + RandomStringUtils.random((int)(50 * Math.random()), "qazwsxedcrfvtgbyhnujmikolp"))
				);
	}

}
